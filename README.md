<center>

# <u>A Gambas3 Compile/Install Utility</u>\
Version "1.5.4"

<img src="http://bws.org.uk/images/GambasUpdatelogo.png">

<h2>For Download/Update and install of the latest<br>
<a href="https://gitlab.com/gambas/gambas">Master or Stable Gambas3 on Gitlab.</a></h2>

## (This software is Beta release)

Written in Gambas basic\
(an older Gambas3 must already be installed)

</center>

<hr width=70%>

#### This utility is for upgrading your gambas basic to the latest version.

**Requirements:**
* Gambas3
* git for git related functions
* Internet connection
* bash shell or another that supports **set -o pipefail** instruction.

**Getting the program**
* Get the source and make the exe with your existing Gambas IDE or use the included CompileAndMakeDesktopIcon script.\
**git clone https://gitlab.com/bsteers4/gambaslatestupdate.git** 


<hr width=70%>

**Supported Linux distributions...**
* Ubuntu (various versions including dev hirsute 21)
* LinuxMint (as ubuntu)
* Debian (stable, unstable, testing)
* Raspbian
* Fedora (latest)
* Archlinux (Manjaro)
* OpenSuse (leap/tumbleweed)

<hr width=70%>

## How to use...
* Launch the application and it should auto-detect your linux version.\

Package dependencies...
* All dependencies/Packages needed to compile gambas are auto-listed and can be auto-installed.\

Gambas source code folder.
* You can git clone a new source download or select an existing folder.
* GitLab can be checked for new commits and git pull used to update.\

Compiling / Installing...
* Process can be started at any point from ./reconf-all to 'make install'
* 'make install' password can be entered at start so you do not have to wait.
* 'Quick Compile' option 
Uninstalling the compiled gambas version.
* It will uninstall any distribution package manager Gambas installation (avoids conflicts).
* Can run 'make uninstall' (removes compiled gambas installation)
* Can run 'make distclean' (cleans source folder for fresh recompile)

Automation.
* 

<table border=1 cellpadding=10 align=center>
<tr><td align=center valign=center colspan=3>Source / Download Window<br><img width=600 src="http://bws.org.uk/images/GambasLatestUpdate.png"> </td></tr>
<tr><td align=center valign=center colspan=3>Compile / Install Window.<br><img width=600 src="http://bws.org.uk/images/GambasLatestUpdateC.png"> </td></tr>
</table>
