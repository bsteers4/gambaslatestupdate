<h1 align=center><u>Gambas Compiler/Installer</u></h1>

Version "1.5.4"

Author: Bruce Steers

#### This utility is for upgrading your gambas basic to the latest version.

<h4>Requirements:</h4>
\* Any older version of Gambas3 above 3.8.\
\* git for git related functions.\
\* Internet connection.\
\* bash shell or another that supports <b>set -o pipefail</b> instruction.

<hr width=70%>
#### Pre-Install steps.

Ensure your repository and system is up to date.\

\* debian/ubuntu/mint\
   \# sudo apt-get update\
   \# sudo apt-get upgrade

\* fedora\
   \# sudo dnf update\
   \# sudo dnf upgrade

\* archLinux / manjaro\
   \# sudo pacman -Syu

\* OpenSuse\
   \# sudo zypper update


## Run the Application.

#### What this program does..\
 (so you do not have to)

\* Your Linux system type will (should) be auto-detected and matched to a sytem listed the gitlab-ci file (if your system is supported). This get's the dependencies and ./configure options needed to compile the latest gambas.\

\* You can manually check for missing needed packages/dependencies and install at any time or it will happen automatically during install\

\* Select a previously downloaded Gambas3 gitlab clone folder  and \'pull\'  updates or 
  \'git clone\' a new folder with a button click.\

\* You can "Check Gitlab" to probe the gitlab gambas commits page for to see the latest commit info and display trunk version.\

\* Displays versions of gambas installed in /usr/bin , the version in the source dir and version on gitlab.\

\* If you wish to use a custom branch other than master and have set the "upstream master" URI you can both git pull and "git pull upstream master"\

\* After getting/updating the source the Compiler/Install page will start the compilation/install process.

<p>

## Advanced Features...

\* Select an alternative gambas gitlab fork (Flavour)\

\* Select unwanted components to not install.\

<hr width=70%>


## Instructions...

#### Initialisation.

On loading the application will first initialise itself. (you must have internet)

If running for the first time it will check your operating system matches one of the supported systems, if not it will open a dialog to manually select a close match (It may or may not work on an unsupported system)

It will check if you have gambas installed already and provide a warning if the installed version is installed via a package manager like apt or zypper. Package manager version will be automatically un-installed during compilation just before install.

It will auto-check you have all dependencies (packages) required for compilation. Dependencies can be installed manually or will be auto-installed before compilation.

#### Setting up...

At the top of the main page you can select an existing Source Folder Path that will be used or the gambas source folder will download into the current selected folder.

An alternative git branch can be selected here.

By default the most important buttons will flash. They are...\
"Check gitlab" , this will show the latest commit message.\
"git pull" , ,this will pull any updates from gitlab.\
"Compile/Install" , press this to go to the compilation page.\

Also on this page you can...\
"upstream master" , this runs "git pull upstream master" used to pull in updates from the gambas master repository if you are using an alternative branch.\

#### ~The Compile/Install page~
#### The 4 steps.

Compilation of gambas has 4 steps...
* ./reconf-all
* ./configure
* make
* make install (must be run as superuser)
Each step can be run in sequence from step 1, or any other step by setting the process start point, also or a single step can be performed (Right click the Begin button for options)

Using the **"./configure q"** option will supress all the "checking" output from the ./configure command and only show the main ouput or errors.

**Password time:** select what to do when the command "sudo make install" is issued.\
On very slow systems it can take a long time to compile gambas then when the "make install"command is run you may not be present at the keyboard to enter the password in time before a timeout occurs.\
Options are.. 
* **Do nothing** this is the normal beaviour, the terminal will ask for password for you to input and will timeout if you are not there.
* **Prompt before install** this will pop a message telling you your password is about to be needed, then on pressing okay the terminal will ask for it.
* **Auto Input** this allows you to enter your password at the start then when it is needed it will be auto inputted for you.

### You are all set, you can now press "Begin" to compile and install gambas.

## Other options

#### ~Quick Compile~
Initially your compile/install will involve all 4 steps to compile but after iniially setting up and in most cases you can simply use the **make** commands to install.

Just running "make" then "make install" will not configure the **version/trunk (gitlab commit hash) information** correctly though.

The "Quick compile" method will first run all 4 compilation steps **only** on the gambas source folder called "**main**" this correctly sets the version strings on the gambas binaries an then it runs make and "make install" on the rest of the source giving a much faster compilation/install time.

#### ~Component List~
With this you can de-select components to compile/install like for example the SDL components or the PDF/Poppler components if you do not use them. This is for gaining speed on the compilation. not all components can be de-selected and some will cause gambas to not function properly so be carefull what you de-select. if not sure then keep it selected. It somewhat crudely renames the component folder names during compilation so they are not found then restores the names on completion.


.
